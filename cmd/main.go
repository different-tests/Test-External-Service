package main

import (
	"context"
	"fmt"
	"io"
	"log"

	"gitlab.com/remotejob/Test-External-Service/pkg/cat"
)


func main() {

	cathandle := cat.NewCat()
	// cathandle.Client{}
	ctx := context.Background()

	filter := cat.CatFilter{Tags: "cute",Limit: 10}

	res,err :=cathandle.List(ctx, &filter)
	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	// b, err := ioutil.ReadAll(resp.Body)  Go.1.15 and earlier
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(string(b))

	


	
}