package cat

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

const (
	host         = "https://cataas.com"
	defaultLimit = 10
)

type CatFilter struct {
	Tags  string
	Skip  int
	Limit int
}
type Cat struct {
	// Client *http.Client // this is client as attribute of the struc
	Client *http.Client
}

func NewCat() *Cat {

	c := http.Client{Timeout: time.Duration(1) * time.Second}

	return &Cat{Client: &c}

}

func (c *Cat) List(ctx context.Context, filter *CatFilter) (*http.Response, error) {
	// no client declaration, use the object from the struct
	if filter == nil {
		filter = &CatFilter{}
	}
	// even if filter is null. keep process it.
	req, err := http.NewRequestWithContext(ctx, "GET", host+"/api/cats", nil)
	if err != nil {
		fmt.Println("error", req, err)
		return nil, err
	}
	limit := defaultLimit
	u := req.URL
	q := u.Query()
	if filter.Tags != "" {
		q.Set("tags", filter.Tags)
	}
	if filter.Skip > 0 {
		q.Set("skip", strconv.Itoa(filter.Skip))
	}
	if filter.Limit > 0 {
		limit = filter.Limit
	}
	q.Set("limit", strconv.Itoa(limit))
	u.RawQuery = q.Encode()

	c.Client.Timeout = time.Duration(1) * time.Second

	return c.Client.Do(req)

}
