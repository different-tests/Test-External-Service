package cat

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"testing"
)

type mockRoundTripper struct {
	mock.Mock // this is where the magic begins
}

func (m *mockRoundTripper) RoundTrip(request *http.Request) (*http.Response, error) {
	args := m.Called(request)
	if args.Get(0) == nil {
		return nil, nil
	}
	return args.Get(0).(*http.Response), args.Error(1)
}

func TestCat_List(t *testing.T) {
	cases := []struct {
		in         *CatFilter
		wantsPath  string
		wantsQuery map[string]string
	}{
		{
			nil,
			"/api/cats",
			map[string]string{
				"limit": "10",
			},
		},
		{
			&CatFilter{
				Tags: "cute",
			},
			"/api/cats",
			map[string]string{
				"tags":  "cute",
				"limit": "10",
			},
		},
		{
			&CatFilter{
				Tags:  "cute",
				Limit: 25,
			},
			"/api/cats",
			map[string]string{
				"tags":  "cute",
				"limit": "25",
			},
		},
		{
			&CatFilter{
				Tags: "blue",
			},
			"/api/cats",
			map[string]string{
				"tags":  "blue",
				"limit": "10",
			},
		},
	}
	service := &Cat{}

	for _, c := range cases {
		mck := &mockRoundTripper{}
		
		mck.On("RoundTrip", mock.AnythingOfType("*http.Request")).Return(&http.Response{}, nil)
	
		// client := &http.Client{Transport: mck}
		service.Client = &http.Client{Transport: mck}
		_, err := service.List(context.Background(), c.in)
		// the sentReq reference will be coming from the mock instead of the response
		// based on the signature, we can retrieve argument from the first argument
		sentReq := mck.Calls[0].Arguments[0].(*http.Request)
		assert.Nil(t, err, c)
		assert.Equal(t, c.wantsPath, sentReq.URL.Path, c)
		for key, value := range c.wantsQuery {
			assert.Equal(t, value, sentReq.URL.Query().Get(key), c)
		}
	}
}
